import { Box, Skeleton, SkeletonText } from "@chakra-ui/react"

export default function UserSkeleton(props){

    return (
        <Box padding='6' boxShadow='lg' bg='white' style={{height: 150, marginBottom: 30}}>
            <Skeleton style={{width: 100, height: 100, float: "left"}}>
                <div style={{width: 100, height: 100, float: "left"}}></div>
            </Skeleton>
            <SkeletonText style={{width: "calc(100% - 120px)", float: "right"}} mt='4' noOfLines={4} spacing='4' skeletonHeight='2' />
        </Box>
    )
}