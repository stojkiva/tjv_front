import React from 'react';
import { Card, CardHeader, CardBody, Button, Heading, Box, Text } from '@chakra-ui/react'
import {
    AlertDialog,
    AlertDialogBody,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogContent,
    AlertDialogOverlay,
    AlertDialogCloseButton,
    useDisclosure
  } from '@chakra-ui/react'
import backendUrl from '../backendUrl';
import {
    Modal,
    useToast,
    Textarea,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    FormControl,
    FormLabel,
    Input
    } from '@chakra-ui/react'
export default function Category(props){

    const { isOpen, onOpen, onClose } = useDisclosure()
    const cancelRef = React.useRef()
    const modal2 = useDisclosure()

    const [name, setName] = React.useState(props.category.name);

    const updateCategory = () => {
        fetch(backendUrl+'/api/categories', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            },
            body: JSON.stringify({"name": name, "oldName": props.category.name}),
            })
            .then(response => response.json())
            .then(data => {
                props.deleted(data['response'] === "SUCCESS")
                onClose()
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    const deleteCategory = () => {
        fetch(backendUrl+'/api/categories', {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"name": props.category.name}),
        })
        .then(response => response.json())
        .then(data => {
            props.deleted(data['response'] === "SUCCESS")
            onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }
    
    return (
        <Card>
            <CardHeader>
                <Heading size='md'>{props.category.name}</Heading>
            </CardHeader>

            <CardBody>
                <Box>
                    <Heading size='xs' textTransform='uppercase'>
                    Summary
                    </Heading>
                    <Text pt='2' fontSize='sm'>
                        Total recipes: {props.category.recipes.length}
                    </Text>
                </Box>
                <br/>
                <Button onClick={modal2.onOpen} variant='outline' colorScheme='green'>
                    Details
                </Button>
                <Button onClick={onOpen} style={{marginLeft: 15}} variant='outline' colorScheme='red'>
                    Delete
                </Button>
            </CardBody>
            <AlertDialog
            isOpen={modal2.isOpen}
            leastDestructiveRef={cancelRef}
            onClose={modal2.onClose}
        >
            <AlertDialogOverlay>
            <AlertDialogContent 
            style={{maxHeight: "70%", overflow: "scroll"}}
            
            >
                <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                    <Input placeholder='name' value={name} onChange={(e) => setName(e.target.value)}/>
                </AlertDialogHeader>

                <AlertDialogBody>
                    <br/>
                    <br/>
                    <br/>
                </AlertDialogBody>

                <AlertDialogFooter>
                <Button ref={cancelRef} variant={"outline"} onClick={updateCategory} colorScheme='blue' style={{marginRight: 10}}>
                    Update
                </Button>
                <Button ref={cancelRef} onClick={modal2.onClose}>
                    Cancel
                </Button>
                </AlertDialogFooter>
            </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
            <AlertDialog
            isOpen={isOpen}
            leastDestructiveRef={cancelRef}
            onClose={onClose}
        >
            <AlertDialogOverlay>
            <AlertDialogContent>
                <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                Delete Category {props.category.id}
                </AlertDialogHeader>

                <AlertDialogBody>
                Are you sure? You can't undo this action afterwards.
                </AlertDialogBody>

                <AlertDialogFooter>
                <Button ref={cancelRef} onClick={onClose}>
                    Cancel
                </Button>
                <Button colorScheme='red' onClick={deleteCategory} ml={3}>
                    Delete
                </Button>
                </AlertDialogFooter>
            </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
        </Card>
    )
}