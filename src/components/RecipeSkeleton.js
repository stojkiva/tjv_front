import { Box, Skeleton, SkeletonText } from "@chakra-ui/react"

export default function RecipeSkeleton(props){

    return (
        <Box padding='6' boxShadow='lg' bg='white' style={{height: 250, marginBottom: 30}}>
            <Skeleton style={{width: 150, height: 190, float: "left"}}>
                <div style={{width: 150, height: 190, float: "left"}}></div>
            </Skeleton>
            <SkeletonText style={{width: "calc(100% - 170px)", float: "right"}} mt='4' noOfLines={8} spacing='4' skeletonHeight='2' />
        </Box>
    )
}