import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  Stack,
  Collapse,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
  Input, InputRightElement, InputGroup, Kbd, propNames
} from '@chakra-ui/react'
import {
  HamburgerIcon,
  CloseIcon,
} from '@chakra-ui/icons'
import * as React from 'react'
import { useHotkeys } from 'react-hotkeys-hook';
import { FaGitlab } from "react-icons/fa";
import { useState } from 'react';

export default function WithSubnavigation(props) {
  const { isOpen, onToggle } = useDisclosure()
  const inputRef = React.useRef();

  const handleFocusInput = () => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  };

  useHotkeys('ctrl+e', (event) => {event.preventDefault();handleFocusInput()});

  const [inputValue, setInputValue] = useState('');
  
  // State to hold the timeout ID
  const [timeoutId, setTimeoutId] = useState(null);

  const handleInputChange = (event) => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    
    const newInputValue = event.target.value;
    setInputValue(newInputValue);
    
    const newTimeoutId = setTimeout(() => {
      props.getData(newInputValue);
    }, 300);
    
    setTimeoutId(newTimeoutId);
  };

  return (
    <Box style={{position: "fixed", width: "100%", zIndex: "1000"}}>
      <Flex
        bg={useColorModeValue('white', 'gray.800')}
        color={useColorModeValue('gray.600', 'white')}
        minH={'60px'}
        py={{ base: 2 }}
        px={{ base: 4 }}
        borderBottom={1}
        borderStyle={'solid'}
        borderColor={useColorModeValue('gray.200', 'gray.900')}
        align={'center'}>
        <Flex
          flex={{ base: 1, md: 'auto' }}
          ml={{ base: -2 }}
          display={{ base: 'flex', md: 'none' }}>
          <IconButton
            onClick={onToggle}
            icon={isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />}
            variant={'ghost'}
            aria-label={'Toggle Navigation'}
          />
        </Flex>
        <Flex flex={{ base: 1 }} justify={{ base: 'center', md: 'start' }}>
          <Text
            style={{marginTop: 10}}
            textAlign={useBreakpointValue({ base: 'center', md: 'left' })}
            fontFamily={'heading'}
            color={useColorModeValue('gray.800', 'white')}>
            WEBSITE
          </Text>

          <div style={{width: "calc(90% - 100px)", marginLeft: "55px", margin: "0 auto"}}>
          <InputGroup>
            <InputRightElement pr={"50px"}>
                <Kbd>Ctrl</Kbd> + <Kbd>E</Kbd>
            </InputRightElement>
            <Input value={inputValue} onChange={handleInputChange} ref={inputRef} placeholder='Search...' />
            </InputGroup>
          </div>
        </Flex>

        <Stack
          flex={{ base: 1, md: 0 }}
          justify={'flex-end'}
          direction={'row'}
          spacing={6}>
          <Button
            as={'a'}
            display={{ base: 'none', md: 'inline-flex' }}
            fontSize={'sm'}
            fontWeight={600}
            color={'white'}
            bg={'pink.400'}
            leftIcon={<FaGitlab/>}
            target='_blank'
            href={'https://gitlab.fit.cvut.cz/stojkiva/tjv_semestral'}
            _hover={{
              bg: 'pink.300',
            }}>
            Repository
          </Button>
        </Stack>
      </Flex>

      <Collapse in={isOpen} animateOpacity>
        Mobile
      </Collapse>
    </Box>
  )
}