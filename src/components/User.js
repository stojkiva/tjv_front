import { Card, CardHeader, Text, CardBody, Button, Heading, Box } from '@chakra-ui/react'
import {
    AlertDialog,
    AlertDialogBody,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogContent,
    AlertDialogOverlay,
    AlertDialogCloseButton,
    useDisclosure
  } from '@chakra-ui/react'
import backendUrl from '../backendUrl'
import {
Modal,
useToast,
Textarea,
ModalOverlay,
ModalContent,
ModalHeader,
ModalFooter,
ModalBody,
ModalCloseButton,
FormControl,
FormLabel,
Input
} from '@chakra-ui/react'

import * as React from "react"

export default function User(props){
    const modal1 = useDisclosure();

    const modal2 = useDisclosure()

    const toast = useToast()

    const cancelRef = React.useRef()
    const c2Ref = React.useRef()

    const [name, setName] = React.useState(props.user.name.split(" ")[0]);
    const [surname, setSurname] = React.useState(props.user.name.split(" ")[1]);

    const [isInvalid, setIsInvalid] = React.useState([false, false, false, false]);

    function validateEmail(email) {
        const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailRegex.test(email);
      }
      
      function validateName(name) {
        const nameRegex = /^[A-Za-z\s]+$/;
        return nameRegex.test(name);
      }


    const updateUser = () => {
        var inv = [false, false, false]

      if (!validateName(name)) {
          inv[0] = true
      }
      
      if (!validateName(surname)) {
        inv[1] = true
      }

      if(inv[0] || inv[1] || inv[2]){
        setIsInvalid(inv)
        toast({
          title: 'Invalid input.',
          description: "Please check all fields.",
          status: 'error',
          duration: 2000,
          isClosable: true,
        });
        return
      }

      fetch(backendUrl+'/api/users', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"email": props.user.email, "name": name+" "+surname}),
        })
        .then(response => response.json())
        .then(data => {
            toast({
              title: 'Account created.',
              description: "We've created your account for you.",
              status: 'success',
              duration: 5000,
              isClosable: true,
            });
            modal2.onClose()
            props.deleted(data['response'] === "SUCCESS")
        })
        .catch(error => {
            console.error('Error:', error);
        });

        modal2.onClose();
    }

    const deleteUser = () => {
        fetch(backendUrl+'/api/users', {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"email": props.user.email}),
        })
        .then(response => response.json())
        .then(data => {
            props.deleted(data['response'] === "SUCCESS")
            modal1.onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    return(
        <Card>
            <CardBody>
                <Box>
                    <div className='userCard'>
                        <div className='userAvatar'></div>
                        <div className='userInfo'>
                            <Heading size='xs' textTransform='uppercase'>
                            {props.user.name}
                            </Heading>
                            <Text pt='2' fontSize='sm'>
                            {props.user.email}
                            </Text>
                            <Text pt='2' fontSize='sm'>
                            Password: {props.user.password}
                            </Text>
                            <br/>
                            <Button onClick={modal2.onOpen} variant='outline' colorScheme='green'>
                                Edit
                            </Button>
                            <Button onClick={modal1.onOpen} style={{marginLeft: 15}} variant='outline' colorScheme='red'>
                                Delete
                            </Button>
                        </div>
                    </div>
                </Box>
            </CardBody>
            <AlertDialog
                isOpen={modal2.isOpen}
                leastDestructiveRef={c2Ref}
                onClose={modal2.onClose}
            >
                <AlertDialogOverlay>
                <AlertDialogContent>
                    <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                    Update User
                    </AlertDialogHeader>

                    <AlertDialogBody>

                        <FormControl>
                        <FormLabel>First name</FormLabel>
                        <Input isInvalid={isInvalid[0]} value={name} onChange={(e) => setName(e.target.value)} placeholder='First name' />
                        </FormControl>

                        <FormControl mt={4}>
                        <FormLabel>Last name</FormLabel>
                        <Input isInvalid={isInvalid[1]} placeholder='Last name' value={surname} onChange={(e) => setSurname(e.target.value)}/>
                        </FormControl>

                    </AlertDialogBody>

                    <AlertDialogFooter>
                    <Button ref={c2Ref} onClick={modal2.onClose}>
                        Cancel
                    </Button>
                    <Button colorScheme='blue' onClick={updateUser} ml={3}>
                        Update
                    </Button>
                    </AlertDialogFooter>
                </AlertDialogContent>
                </AlertDialogOverlay>
            </AlertDialog>
            <AlertDialog
                isOpen={modal1.isOpen}
                leastDestructiveRef={cancelRef}
                onClose={modal1.onClose}
            >
                <AlertDialogOverlay>
                <AlertDialogContent>
                    <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                    Delete User
                    </AlertDialogHeader>

                    <AlertDialogBody>
                    Are you sure? You can't undo this action afterwards.
                    </AlertDialogBody>

                    <AlertDialogFooter>
                    <Button ref={cancelRef} onClick={modal1.onClose}>
                        Cancel
                    </Button>
                    <Button colorScheme='red' onClick={deleteUser} ml={3}>
                        Delete
                    </Button>
                    </AlertDialogFooter>
                </AlertDialogContent>
                </AlertDialogOverlay>
            </AlertDialog>
        </Card>
    )
}