import { Card, CardHeader, Text, CardBody, CardFooter, Heading, Box, Badge, Image, Stack, Button } from '@chakra-ui/react'
import {
    AlertDialog,
    AlertDialogBody,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogContent,
    AlertDialogOverlay,
    Select,
    useToast,
    Input,
    AlertDialogCloseButton,
    useDisclosure
  } from '@chakra-ui/react'
import * as React from 'react'
import backendUrl from '../backendUrl'

export default function Recipe(props){
    const modal1 = useDisclosure()
    const modal2 = useDisclosure()

    const [text, setText] = React.useState("")

    const [name, setName] = React.useState(props.recipe.name)
    const [content, setContent] = React.useState(props.recipe.content)

    const toast = useToast()

    const [user, setUser] = React.useState("")
    const cancelRef = React.useRef()

    const addLike = () => {
        var us
        if(props.users.length > 0 && user == "") {
            us = props.users[0].email
        }else{
            us = user
        }
        if(us == ""){
            toast({
                title: 'Invalid input.',
                description: "Please check all fields.",
                status: 'error',
                duration: 2000,
                isClosable: true,
              });
              return
        }
        fetch(backendUrl+'/api/recipes/like', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"recipeId": props.recipe.id, "userId": us}),
        })
        .then(response => response.json())
        .then(data => {
            props.deleted(true)
            modal1.onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }
    
    const deleteLike = (userId) => {
        var us
        if(props.users.length > 0 && user == "") {
            us = props.users[0].email
        }else{
            us = user
        }
        if(us == ""){
            toast({
                title: 'Invalid input.',
                description: "Please check all fields.",
                status: 'error',
                duration: 2000,
                isClosable: true,
              });
              return
        }
        fetch(backendUrl+'/api/recipes/like', {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"recipeId": props.recipe.id, "userId": us}),
        })
        .then(response => response.json())
        .then(data => {
            props.deleted(true)
            modal1.onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    const updateRecipe = () => {
        fetch(backendUrl+'/api/recipes', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            },
            body: JSON.stringify({"id": props.recipe.id,"name": name, "content": content}),
            })
            .then(response => response.json())
            .then(data => {
                props.deleted(true)
                modal2.onClose()
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    const deleteComment = (commentId) => {
        fetch(backendUrl+'/api/recipes/comment', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            },
            body: JSON.stringify({"commentId": commentId}),
            })
            .then(response => response.json())
            .then(data => {
                props.deleted(true)
                modal1.onClose()
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    const addComment = () => {
        var us
        if(props.users.length > 0 && user == "") {
            us = props.users[0].email
        }else{
            us = user
        }
        console.log(text.length)
        if(us == "" || text.length == 0){
            toast({
                title: 'Invalid input.',
                description: "Please check all fields.",
                status: 'error',
                duration: 2000,
                isClosable: true,
              });
              return
        }
        fetch(backendUrl+'/api/recipes/comment', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"recipeId": props.recipe.id, "userId": us, "text": text}),
        })
        .then(response => response.json())
        .then(data => {
            props.deleted(true)
            modal1.onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    const deleteRecipe = () => {
        fetch(backendUrl + '/api/recipes', {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"id": props.recipe.id}),
        })
        .then(response => response.json())
        .then(data => {
            props.deleted(data['response'] === "SUCCESS")
            modal1.onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    return (
        <Card
        direction={{ base: 'column', sm: 'row' }}
        overflow='hidden'
        variant='outline'
        >
        <Image
            objectFit='cover'
            maxW={{ base: '100%', sm: '200px' }}
            src={backendUrl+"/images/"+props.recipe.thumbnail+"?v=1"}
            alt='Caffe Latte'
        />

        <Stack>
            <CardBody>
            <Heading size='md'>
                {props.recipe.name}
            </Heading>
            <Badge variant='subtle' colorScheme='blue'>
                {props.recipe.category.name}
            </Badge>

            <Text py='2'>
                {props.recipe.content.slice(0, 50)}...
            </Text>
            <Text py='2'>
                Comments: {props.recipe.comments.length}
            </Text>
            <Text py='2'>
                Likes: {props.recipe.likedBy.length}
            </Text>
            </CardBody>

            <CardFooter>
            <Button onClick={modal2.onOpen} variant='outline' colorScheme='green'>
                Details
            </Button>
            <Button onClick={modal1.onOpen} style={{marginLeft: 15}} variant='outline' colorScheme='red'>
                Delete
            </Button>
            </CardFooter>
        </Stack>
        <AlertDialog
            isOpen={modal2.isOpen}
            leastDestructiveRef={cancelRef}
            onClose={modal2.onClose}
        >
            <AlertDialogOverlay>
            <AlertDialogContent 
            style={{maxHeight: "70%", overflow: "scroll"}}
            
            >
                <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                    <Input placeholder='name' value={name} onChange={(e) => setName(e.target.value)}/>
                </AlertDialogHeader>

                <AlertDialogBody>
                    <Input placeholder='content' value={content} onChange={(e) => setContent(e.target.value)}/>
                    <br/>
                    <Button onClick={updateRecipe} variant={"outline"} colorScheme='blue'>Update</Button>
                    <br/>
                    <br/>
                    <span style={{fontSize: 20}}>Comments:</span>
                    <Select value={user} onChange={(e) => {
                        if(e.target.value !== ""){
                        setUser(e.target.value)
                        }
                    }} className='select'>
                        {props.users.map((user) => (
                        <option value={user.email} key={user.email}>{user.email}</option>
                        ))}
                    </Select>
                    <br/>
                    <Input placeholder='text' value={text} onChange={(e) => setText(e.target.value)}/>
                    <br/>
                    <br/>
                    <Button variant={"outline"} onClick={addComment} colorScheme='blue'>Add Comment</Button>
                    {props.recipe.comments.map((comment) => (
                        <Stack style={{border: "1px solid #E2E8F0", padding: 5, borderRadius: 5, marginTop: 10}} key={comment.id}>
                            <CardBody>
                            <Heading size='md'>
                                {comment.user.name} 
                            </Heading>
                            <Text py='2'>
                                {comment.text}
                            </Text>
                            </CardBody>
                
                            <CardFooter>
                            <Button onClick={() => {deleteComment(comment.id)}} variant='outline' colorScheme='red'>
                                Delete
                            </Button>
                            </CardFooter>
                        </Stack>
                    ))}
                    <br/>
                    <span style={{fontSize: 20}}>Likes:</span>
                    <Select value={user} onChange={(e) => {
                        if(e.target.value !== ""){
                        setUser(e.target.value)
                        }
                    }} className='select'>
                        {props.users.map((user) => (
                        <option value={user.email} key={user.email}>{user.email}</option>
                        ))}
                    </Select>
                    <br/>
                    <br/>
                    <Button variant={"outline"} onClick={addLike} colorScheme='blue'>Add Like</Button>
                    {props.recipe.likedBy.map((like) => (
                        <Stack style={{border: "1px solid #E2E8F0", padding: 5, borderRadius: 5, marginTop: 10}} key={like.id}>
                            <CardBody>
                            <Heading size='md'>
                                {like.email} 
                            </Heading>
                            <Button onClick={() => {deleteLike(like.email)}} variant='outline' colorScheme='red'>
                                Delete
                            </Button>
                            </CardBody>
                        </Stack>
                    ))}
                </AlertDialogBody>

                <AlertDialogFooter>
                <Button ref={cancelRef} onClick={modal2.onClose}>
                    Cancel
                </Button>
                </AlertDialogFooter>
            </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
        <AlertDialog
            isOpen={modal1.isOpen}
            leastDestructiveRef={cancelRef}
            onClose={modal1.onClose}
        >
            <AlertDialogOverlay>
            <AlertDialogContent>
                <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                Delete Recipe {props.recipe.id}
                </AlertDialogHeader>

                <AlertDialogBody>
                Are you sure? You can't undo this action afterwards.
                </AlertDialogBody>

                <AlertDialogFooter>
                <Button ref={cancelRef} onClick={modal1.onClose}>
                    Cancel
                </Button>
                <Button colorScheme='red' onClick={deleteRecipe} ml={3}>
                    Delete
                </Button>
                </AlertDialogFooter>
            </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
        </Card>
    )
}