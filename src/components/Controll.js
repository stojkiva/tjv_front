import * as React from 'react'
import { Select, Button, useDisclosure } from '@chakra-ui/react'
import {
  Modal,
  useToast,
  Textarea,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
} from '@chakra-ui/react'
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/react'
import backendUrl from '../backendUrl';
function validateEmail(email) {
  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  return emailRegex.test(email);
}

function validateName(name) {
  const nameRegex = /^[A-Za-z\s]+$/;
  return nameRegex.test(name);
}

export default function Controll(props){
    const { isOpen, onOpen, onClose } = useDisclosure(true)
    const modal2 = useDisclosure()
    
    const display = props.display
    const setDisplay = (e) => {
      setName("")
      setEmail("")
      setSurname("")
      setIsInvalid([false, false, false, false])
      props.changedDisplay(e)
    }
    const initialRef = React.useRef(null)
    const finalRef = React.useRef(null)
    const cancelRef = React.useRef(null)
    const [file, setFile] = React.useState(null)
    const toast = useToast()
    const [name, setName] = React.useState("")
    const [surname, setSurname] = React.useState("")
    const [email, setEmail] = React.useState("")
    const [text, setText] = React.useState("")
    const [user, setUser] = React.useState("")
    const [recipes, setRecipes] = React.useState([])
    const [categoryComp, setCategoryComp] = React.useState("")
    const [category, setCategory] = React.useState((props.categories != null && props.categories.length > 0) ? props.categories[0] : "")

    const [isInvalid, setIsInvalid] = React.useState([false, false, false, false]);

    const changeFile = (e) => {
      setFile(null)
      console.log(e.target.files[0])

      const formData = new FormData();
      formData.append('file', e.target.files[0]);

      fetch(backendUrl+'/images/upload', {
        method: 'POST',
        body: formData,
      })
      .then(response => {
        return response.json();
      })
      .then(data => {
        if(data.result != "SUCCESS"){
          toast({
            title: 'Invalid image.',
            description: "Please select a valid image.",
            status: 'error',
            duration: 2000,
            isClosable: true,
          });
          return;
        }
        toast({
          title: 'Image uploaded succesfully.',
          description: "Please select a valid image.",
          status: 'success',
          duration: 2000,
          isClosable: true,
        });
        setFile(data.file)
        // Handle success response from the backend
      })
      .catch(error => {
        toast({
          title: 'Invalid image.',
          description: "Please select a valid image.",
          status: 'error',
          duration: 2000,
          isClosable: true,
        });
      });
    }

    const addCategory = () => {
      var inv = [false, false, false];

      if(!validateName(name)){
        inv[0] = true
      }

      if(inv[0]){
        setIsInvalid(inv)
        toast({
          title: 'Invalid input.',
          description: "Please check all fields.",
          status: 'error',
          duration: 2000,
          isClosable: true,
        });
        return
      }

      fetch('http://localhost:8080/api/categories', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"name": name}),
        })
        .then(response => response.json())
        .then(data => {
            toast({
              title: 'Category created.',
              description: "We've created your account for you.",
              status: 'success',
              duration: 5000,
              isClosable: true,
            });
            onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });

    }

    const addRecipe = () => {

      var inv = [false, false, false]

      if (!validateName(name)) {
          inv[0] = true
      }
      var cat = category;
      if(props.categories.length != 0 || category == ""){
        cat = props.categories[0].name
      }

      if(file == null || inv[0] || props.categories.length < 1){
        setIsInvalid(inv)
        toast({
          title: 'Invalid input.',
          description: "Please check all fields.",
          status: 'error',
          duration: 2000,
          isClosable: true,
        });
        return
      }

      fetch(backendUrl+'/api/recipes', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"name": name, "text": text, "thumbnail": file, "category": cat}),
        })
        .then(response => response.json())
        .then(data => {
            toast({
              title: 'Account created.',
              description: "We've created your account for you.",
              status: 'success',
              duration: 5000,
              isClosable: true,
            });
            onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }
    
    const complexQuery = () => {
      var cmp = "" 
      if(categoryComp == "" && props.categories.length != 0){
        cmp = props.categories[0].name;
      }

      var usr = ""
      if(user == "" && props.users.length != 0){
        usr = props.users[0].email
      }

      if(usr == "" || cmp == ""){
        toast({
          title: 'Invalid input.',
          description: "Please select user and category",
          status: 'error',
          duration: 5000,
          isClosable: true,
        });
        return
      }
      
      fetch(backendUrl+'/api/recipes/complex', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"email": usr, "category": cmp}),
        })
        .then(response => response.json())
        .then(data => {
            toast({
              title: 'Complex query performed.',
              description: "Size of response: "+data.length+" recipes",
              status: 'success',
              duration: 5000,
              isClosable: true,
            });
            setRecipes(data)
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    const addUser = () => {

      var inv = [false, false, false]

      if (!validateEmail(email)) {
          inv[2] = true
      }

      if (!validateName(name)) {
          inv[0] = true
      }
      
      if (!validateName(surname)) {
        inv[1] = true
      }

      if(inv[0] || inv[1] || inv[2]){
        setIsInvalid(inv)
        toast({
          title: 'Invalid input.',
          description: "Please check all fields.",
          status: 'error',
          duration: 2000,
          isClosable: true,
        });
        return
      }

      fetch(backendUrl+'/api/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': "*"
        },
        body: JSON.stringify({"email": email, "name": name+" "+surname, "password": "heheheh"}),
        })
        .then(response => response.json())
        .then(data => {
            toast({
              title: 'Account created.',
              description: "We've created your account for you.",
              status: 'success',
              duration: 5000,
              isClosable: true,
            });
            onClose()
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }

    return(
        <div className='buttonsHolder'>
          <div className='selectHolder'>
            <Select value={display} onChange={(e) => {
              if(e.target.value !== ""){
                setDisplay(e.target.value)
              }
            }} className='select'>
              <option value='users'>Users</option>
              <option value='recipes'>Recipes</option>
              <option value='categories'>Categories</option>
            </Select>
          </div>
          <div className='buttonDiv'>
          <Button colorScheme='teal' className='addButton' onClick={(onOpen)} variant='outline'>
            Add Record
          </Button>
          </div>
          <div>
          <Button colorScheme='blue' style={{marginTop: 20, float: "left"}} onClick={modal2.onOpen} variant={"outline"}>
            Complex Query
          </Button>
          </div>
          <AlertDialog
            isOpen={modal2.isOpen}
            leastDestructiveRef={cancelRef}
            onClose={modal2.onClose}
        >
            <AlertDialogOverlay>
            <AlertDialogContent 
            style={{maxHeight: "70%", overflow: "scroll"}}
            
            >
                <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                  Complex Query (All recipes in category that has a comment by user)
                  <Select value={user} onChange={(e) => {
                      if(e.target.value !== ""){
                      setUser(e.target.value)
                      }
                  }} className='select'>
                      {
                        props.users != null ? 
                        <>
                          {props.users.map((user) => (
                          <option value={user.email} key={user.email}>{user.email}</option>
                          ))}
                        </>
                        :
                        <></>
                      }
                  </Select>
                  <br/>
                  <Select value={categoryComp} onChange={(e) => {
                      if(e.target.value !== ""){
                      setCategoryComp(e.target.value)
                      }
                  }} className='select'>
                      {
                        props.categories != null ? 
                        <>
                          {props.categories.map((category) => (
                          <option value={category.name} key={category.name}>{category.name}</option>
                          ))}
                        </>
                        :
                        <></>
                      }
                  </Select>
                </AlertDialogHeader>

                <AlertDialogBody>
                    {recipes.map((recipe) => (
                      <div key={recipe.id}>
                        <h2 style={{fontSize: 20, fontWeight: 700}}>{recipe.name}</h2>
                        {recipe.content}
                      </div>
                    ))}
                </AlertDialogBody>

                <AlertDialogFooter>
                <Button ref={cancelRef} style={{marginRight: 10}} onClick={complexQuery}>
                    Run
                </Button>
                <Button ref={cancelRef} onClick={modal2.onClose}>
                    Cancel
                </Button>
                </AlertDialogFooter>
            </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
          <Modal
            initialFocusRef={initialRef}
            finalFocusRef={finalRef}
            isOpen={isOpen}
            onClose={onClose}
          >
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Create new account</ModalHeader>
              <ModalCloseButton />
              <ModalBody pb={6}>
                {
                  display === "users" ?
                  <>
                    <FormControl>
                      <FormLabel>First name</FormLabel>
                      <Input isInvalid={isInvalid[0]} ref={initialRef} value={name} onChange={(e) => setName(e.target.value)} placeholder='First name' />
                    </FormControl>

                    <FormControl mt={4}>
                      <FormLabel>Last name</FormLabel>
                      <Input isInvalid={isInvalid[1]} placeholder='Last name' value={surname} onChange={(e) => setSurname(e.target.value)}/>
                    </FormControl>
                    <FormControl mt={4}>
                      <FormLabel>Email</FormLabel>
                      <Input isInvalid={isInvalid[2]} placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)}/>
                    </FormControl>
                  </>
                  :
                  <>
                    {
                      display === "categories" ? 
                      <>
                        <FormControl mt={4}>
                          <FormLabel>Name</FormLabel>
                          <Input isInvalid={isInvalid[0]} placeholder='Name' value={name} onChange={(e) => setName(e.target.value)}/>
                        </FormControl>
                      </>
                      :
                      <>
                        <FormControl mt={4}>
                          <FormLabel>Name</FormLabel>
                          <Input isInvalid={isInvalid[0]} placeholder='Name' value={name} onChange={(e) => setName(e.target.value)}/>
                        </FormControl>
                        <FormControl mt={4}>
                          <FormLabel>Text</FormLabel>
                          <Textarea resize={"none"} height={175} isInvalid={isInvalid[1]} placeholder='Text' value={text} onChange={(e) => setText(e.target.value)} />
                        </FormControl>
                        <FormControl mt={4}>
                          <FormLabel>Thumbnail</FormLabel>
                          <Input onChange={changeFile} placeholder='thumbnail' type="file"/>
                        </FormControl>

                        <FormControl mt={4}>
                          <FormLabel>Category</FormLabel>
                          <Select value={category} onChange={(e) => {
                            if(e.target.value !== ""){
                              setCategory(e.target.value)
                            }
                          }} className='select'>
                            {props.categories.map((cat) => (
                              <option value={cat.name} key={cat.name}>{cat.name}</option>
                            ))}
                          </Select>
                        </FormControl>
                      </>
                    }
                  </>
                }
              </ModalBody>

              <ModalFooter>
                <Button colorScheme='blue' mr={3} onClick={() =>{
                      if(display === "users"){
                        addUser()
                      }
                      if(display === "categories"){
                        addCategory()
                      }
                      if(display === "recipes"){
                        addRecipe()
                      }
                      props.deleted()
                    }
                  }>
                  Save
                </Button>
                <Button onClick={onClose}>Cancel</Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </div>
    )
}