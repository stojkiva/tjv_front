import * as React from 'react'

// 1. import `ChakraProvider` component
import { ChakraProvider } from '@chakra-ui/react'
import { Heading } from '@chakra-ui/react'
import { Text } from '@chakra-ui/react'
import {Table, TableContainer, TableCaption, Thead, Tr, Th, Td, Tbody, Tfoot, Button} from '@chakra-ui/react'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
} from '@chakra-ui/react'
import { useRef } from 'react'
import { useDisclosure } from '@chakra-ui/react'
import { Divider } from '@chakra-ui/react'
import { useToast } from '@chakra-ui/react'
import { Select } from '@chakra-ui/react'
import { useState } from 'react'

function App() {
  const { isOpen, onOpen, onClose } = useDisclosure(true)
  const initialRef = React.useRef(null)
  const [display, setDisplay] = useState("users")
  const finalRef = React.useRef(null)
  const toast = useToast()

  return (
    <ChakraProvider>
      <div className='mainDiv'>
        <Heading>TJV semestral</Heading>
        <Text fontSize='md'>Че писать непонятно</Text>
        <br/>
        <Divider/>
        <div className='buttonsHolder'>
          <div className='selectHolder'>
            <Select placeholder='Select Entity' className='select'>
              <option value='option1'>Users</option>
              <option value='option2'>Recipes</option>
              <option value='option3'>Comments</option>
            </Select>
          </div>
          <div className='buttonDiv'>
          <Button colorScheme='teal' className='addButton' onClick={(onOpen)} variant='outline'>
            Add User
          </Button>
          </div>
        </div>
        <TableContainer>
        <Table variant='simple'>
          <TableCaption>Imperial to metric conversion factors</TableCaption>
          <Thead>
            <Tr>
              <Th>To convert</Th>
              <Th>into</Th>
              <Th isNumeric>multiply by</Th>
            </Tr>
          </Thead>
          <Tbody>
            <Tr>
              <Td>inches</Td>
              <Td>millimetres (mm)</Td>
              <Td isNumeric>25.4</Td>
            </Tr>
            <Tr>
              <Td>feet</Td>
              <Td>centimetres (cm)</Td>
              <Td isNumeric>30.48</Td>
            </Tr>
            <Tr>
              <Td>yards</Td>
              <Td>metres (m)</Td>
              <Td isNumeric>0.91444</Td>
            </Tr>
          </Tbody>
          <Tfoot>
            <Tr>
              <Th>To convert</Th>
              <Th>into</Th>
              <Th isNumeric>multiply by</Th>
            </Tr>
          </Tfoot>
        </Table>
      </TableContainer>
      </div>
    </ChakraProvider>
  )
}

export default App;