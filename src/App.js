import * as React from 'react'
import { ChakraProvider, InputLeftElement, Kbd, Box } from '@chakra-ui/react'
import { Heading } from '@chakra-ui/react'
import { Text } from '@chakra-ui/react'
import { Divider } from '@chakra-ui/react'
import { useState } from 'react'
import Controll from './components/Controll'
import User from './components/User'
import Recipe from './components/Recipe'
import UserSkeleton from './components/UserSkeleton'
import RecipeSkeleton from './components/RecipeSkeleton'
import Navbar from './components/Navbar'
import Category from './components/Category'
import { Skeleton, SkeletonCircle, SkeletonText } from '@chakra-ui/react'
import backendUrl from './backendUrl'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function App() {
  const [display, setDisplay] = useState("users")
  const [users, setUsers] = useState(null)
  const [recipes, setRecipes] = useState(null)
  const [categories, setCategories] = useState(null)
  const initialRef = React.useRef(null)
  const cancelRef = React.useRef(null)
  
  const changedDisplay = async (disp) => {
    setDisplay(disp)
  }

  const getData = async (search = "") => {
    
    var url = backendUrl + "/api/" + display

    if(search != ""){
      url += "/"+search
    }
    await sleep(300)
    fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if(display === "users"){
        setUsers(data);
      }
      if(display === "recipes"){
        setRecipes(data);
      }
      if(display === "categories"){
        setCategories(data);
      }
    });

    var url = backendUrl + "/api/categories"
    fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      setCategories(data);
    });

  }

  const deleted = (success = true) => {
    getData()
  }
  React.useEffect(()=>{
    getData()
  },[display])

  return (
    <ChakraProvider>
      <Navbar getData={getData}/>
      <div className='mainDiv'>
        <Heading>TJV semestral</Heading>
        <Text fontSize='md'>Choose an Entity to Begin. Use <Kbd>Ctrl</Kbd> + <Kbd>E</Kbd> for searching.</Text>
        <br/>
        <Divider/>
        <Controll users={users}changedDisplay={changedDisplay} deleted={deleted} categories={categories} display={display} setDisplay={setDisplay}/>
        <br/>
        <br/>
        <div>
          {display === "users" ? 
            <>
              {users === null ? 
                <>
                  <UserSkeleton/>
                  <UserSkeleton/>
                  <UserSkeleton/>
                </>
                :
                <>
                  {users.map((user, index) => (
                    <div key={index}>
                      <User deleted={deleted} key={index} user={user}/>
                      <br/>
                    </div>
                  ))}
                </>
              }
            </>
            :
            <>
              {
              display === "recipes" ? 
              <>
                {
                  recipes === null ? 
                  <>
                    <RecipeSkeleton/>
                  </>
                  :
                  <>
                    {recipes.map((recipe, index) => (
                      <div key={index}>
                        <Recipe users={users} deleted={deleted} recipe={recipe}/>
                        <br/>
                      </div>
                    ))}
                  </>
                }
              </>
              :
              <>
              {categories === null ? 
                <>
                  <UserSkeleton/>
                  <UserSkeleton/>
                  <UserSkeleton/>
                </>
                :
                <>
                  {categories.map((category, index) => (
                    <div key={index}>
                      <Category deleted={deleted} category = {category}/>
                      <br/>
                    </div>
                  ))}
                </>
              }
            </>
            }
            </>
          }
        </div>
      </div>
    </ChakraProvider>
  )
}

export default App;